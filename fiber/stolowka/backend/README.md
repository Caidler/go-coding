# Canteen Project

Hi! This is a README file dedicated to the descrpition of a canteen project created in Fiber, which is a GO web framework built on Fasthttp.

## Getting Started

TODO: Install postgres, golang, create .env file.

---

## What is a SPA?

A **SPA** is a **Single-Page Application**. Such applications run inside browser and don't necessitate page reloading during use. We can encounter such applications daily, e.g. Facebook, Google Maps, etc.
**SPA** renders pages directly in the browser. We can do this using JavaScript frameworks, such as: Angular.js or Knockout.js. **SPAs** offer their users a smooth and stress-free experience without any hassle as their content is presented to them is a comfortable and easily accessable form.
Do **SPAs** have any disadvantages then? Surely, one of the greatest ones is the trickiness behind SEO optimisation as their contents are loaded by AJAX (Asynchronous JavaScript and XML).
At the beginning of Google crawlers' activity, they only had to index the text-based content written in HTML. Nontheless, with the growing dominance of JavaScript is became increasingly more problematic. When a **SPA** contains an external JavaScript file inserted into HTML using the `<script>` attribute, the browser runs the script and loads its contents dynamicallly on the website. A crawler may fail to perform this operaton and in turn index the application as an emtpy page.

### Is there a solution to this conundrum?

Well, partly yes. Firstly, Google crawlers have apparently be getting updates to make their ability to index JavaScript websites better. Secondly, and probably also more importantly, these days one can use SSR (server-side rendering). This is a method of loading your application's JavaScript on your server. When a user or a Google crawler requests your website, it is prestented to them as a static HTML page. Thanks to this, your application can be easily indexed.
SSR has also other benefits, e.g. if your content is shared across social media platforms, such as Facebook or Twitter, the post includes a preview of your page.

### Why is it improtant for this project?

This project uses _Fiber_ which is a framework heavily insipired by _Express_, the most popular _Node.js_ framework. _Fiber_ allows for server-side rendering which is used in the present project. It makes it easy for people with _Node.js_ and _Express_ experience to switch to _Go_. As mentioned before, it is also uses **fasthttp** instead of **net/http** and this is what we're about to investigate next.

## Why fasthttp?

At the onset of the project, a chocie had to be made whether to use a **fasthttp** or **net/http** based server. I decided to opt for the fasthttp option as according to benchmarks in can be up to **10 times** faster than its counterpart.

### Why is it so much faster? And is it always good?

Bascially, the worker pool model is zero allocation. The workers are initialised before the loop. However, the question remains - is it always the right choice?
The answer appears to be "no". **fasthttp** is not a direct replacement for **net/http** as the function singuatures are not the same. The APIs are not compatible, thus you need much more documentation. It doesn't support HTTP/2. **net/http** is surely more battle-tested and is subject to more scruitny. Therefore, it should mostly be used for projects which can expect an extremely high volume of requests.

## Why is it used in this project?

The main purpose behind this project was to aquaint myself with **golang** and the **Fiber** framework in my quest to find the best possible solutions for my professional projects.