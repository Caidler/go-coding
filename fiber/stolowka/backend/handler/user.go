package handler

import (
	"stolowka-api/database"
	"stolowka-api/model"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// func validToken(t *jwt.Token, id string) bool {
// 	n, err := strconv.Atoi(id)
// 	if err != nil {
// 		return false
// 	}

// 	claims := t.Claims.(jwt.MapClaims)
// 	uid := int(claims["user_id"].(float64))

// 	return uid == n
// }

func validToken(t *jwt.Token, id string) bool {
	return t.Claims.(jwt.MapClaims)["user_id"] == id
}

func validUser(id string, p string) bool {
	db := database.DB
	var user model.User
	db.First(&user, "id = ?", id)
	if user.Username == "" {
		return false
	}
	if !CheckPasswordHash(p, user.Password) {
		return false
	}
	return true
}

// GetUser get a user
func GetUser(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DB
	user := model.User{}
	db.Find(&user, "id = ?", id)
	if user.Username == "" {
		return c.Status(404).JSON(fiber.Map{"message": "No user found with ID", "data": nil})
	}
	return c.JSON(fiber.Map{"message": "User found", "data": user})
}

// CreateUser new user
func CreateUser(c *fiber.Ctx) error {
	// Defining structs of body and Response.
	type IncomingData struct {
		Username  string `json:"username"`
		Email     string `json:"email"`
		Password  string `json:"password"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
	}
	type Response struct {
		Username string `json:"username"`
		Email    string `json:"email"`
	}

	// Parsing and reading Data
	data := IncomingData{}
	if err := c.BodyParser(&data); err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Review your input, unable to parse body", "data": err})
	}

	// OPTIONS: Check if password is longer than 8 digits, anything for real...
	if data.Username == "" || data.Email == "" || data.Password == "" {
		return c.Status(400).JSON(fiber.Map{"message": "Username, Password and Email are required!", "data": nil})
	}

	// Connecting to DB and creating user
	db := database.DB
	user := new(model.User)

	// Creating UUID and checking if user with created UUID exists in the database, if exists repeats until unique uuid is created.
	var count int8 = -1
	for count < 0 {
		user.ID = uuid.New().String()
		db.First(&user, "id = ?", user.ID).Count(&count)
	}

	// Hashing Password.
	hash, err := hashPassword(data.Password)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Couldn't hash password", "data": err})
	}

	// Creating user fields
	user.Password = hash
	user.Username = data.Username
	user.Email = data.Email
	user.FirstName = data.FirstName
	user.LastName = data.LastName
	user.Scope = "basic"

	// Writing user to database.
	if err := db.Create(&user).Error; err != nil {
		// TODO: we should make some fatal logs here instead of returning err data to the user. Needs to be discussed.
		return c.Status(500).JSON(fiber.Map{"message": "Couldn't create user", "data": err})
	}

	// Creating response and responding
	newUser := Response{
		Email:    data.Email,
		Username: data.Username,
	}

	return c.JSON(fiber.Map{"message": "Created user", "data": newUser})
}

// CreateAdminUser new user
func CreateAdminUser(c *fiber.Ctx) error {
	// Defining structs of body and Response.
	type IncomingData struct {
		Username  string `json:"username"`
		Email     string `json:"email"`
		Password  string `json:"password"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
	}
	type Response struct {
		Username string `json:"username"`
		Email    string `json:"email"`
	}

	// Parsing and reading Data
	data := IncomingData{}
	if err := c.BodyParser(&data); err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Review your input, unable to parse body", "data": err})
	}

	// OPTIONS: Check if password is longer than 8 digits, anything for real...
	if data.Username == "" || data.Email == "" || data.Password == "" {
		return c.Status(400).JSON(fiber.Map{"message": "Username, Password and Email are required!", "data": nil})
	}

	// Connecting to DB and creating user
	db := database.DB
	user := new(model.User)

	// Creating UUID and checking if user with created UUID exists in the database, if exists repeats until unique uuid is created.
	var count int8 = -1
	for count < 0 {
		user.ID = uuid.New().String()
		db.First(&user, "id = ?", user.ID).Count(&count)
	}

	// Hashing Password.
	hash, err := hashPassword(data.Password)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Couldn't hash password", "data": err})
	}

	// Creating user fields
	user.Password = hash
	user.Username = data.Username
	user.Email = data.Email
	user.FirstName = data.FirstName
	user.LastName = data.LastName
	user.Scope = "basic;admin"

	// Writing user to database.
	if err := db.Create(&user).Error; err != nil {
		// TODO: we should make some fatal logs here instead of returning err data to the user. Needs to be discussed.
		return c.Status(500).JSON(fiber.Map{"message": "Couldn't create user", "data": err})
	}

	// Creating response and responding
	newUser := Response{
		Email:    data.Email,
		Username: data.Username,
	}

	return c.JSON(fiber.Map{"message": "Created user", "data": newUser})
}

// UpdateUser update user
// func UpdateUser(c *fiber.Ctx) error {
// 	type UpdateUserInput struct {
// 		Names string `json:"names"`
// 	}
// 	var uui UpdateUserInput
// 	if err := c.BodyParser(&uui); err != nil {
// 		return c.Status(500).JSON(fiber.Map{"message": "Review your input", "data": err})
// 	}
// 	id := c.Params("id")
// 	token := c.Locals("user").(*jwt.Token)

// 	if !validToken(token, id) {
// 		return c.Status(500).JSON(fiber.Map{"message": "Invalid token id", "data": nil})
// 	}

// 	db := database.DB
// 	var user model.User

// 	db.First(&user, id)
// 	user.Names = uui.Names
// 	db.Save(&user)

// 	return c.JSON(fiber.Map{"message": "User successfully updated", "data": user})
// }

// // DeleteUser delete user
func DeleteUser(c *fiber.Ctx) error {
	type PasswordInput struct {
		Password string `json:"password"`
	}
	pi := PasswordInput{}
	if err := c.BodyParser(&pi); err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Review your input", "data": err})
	}
	id := c.Params("id")
	token := c.Locals("user").(*jwt.Token)

	if !validToken(token, id) {
		return c.Status(500).JSON(fiber.Map{"message": "Invalid token id", "data": nil})
	}

	if !validUser(id, pi.Password) {
		return c.Status(500).JSON(fiber.Map{"message": "Provided wrong password", "data": nil})
	}

	db := database.DB
	var user model.User

	db.Find(&user, "id = ?", id)
	db.Delete(&user)
	return c.JSON(fiber.Map{"message": "User successfully deleted", "data": nil})
}

// Get Data from token
func GetMyData(c *fiber.Ctx) error {
	token := c.Locals("user").(*jwt.Token)

	uid := token.Claims.(jwt.MapClaims)["user_id"]

	db := database.DB
	user := model.User{}

	// retrive whole user data.
	// TODO: We need SELECT only desired fields, not whole user.
	db.Find(&user, "id = ?", uid)
	type Response struct {
		ID          string    `json:"id"`
		Username    string    `json:"username"`
		Email       string    `json:"email"`
		FirstName   string    `json:"firstName"`
		LastName    string    `json:"lastName"`
		NewsLetters []string  `json:"newsLetters"`
		CreatedAt   time.Time `json:"createdAt"`
		UpdatedAt   time.Time `json:"updatedAt"`
	}
	response := Response{}
	response.ID = user.ID
	response.Username = user.Username
	response.Email = user.Email
	response.FirstName = user.FirstName
	response.LastName = user.LastName
	response.CreatedAt = user.CreatedAt
	response.UpdatedAt = user.UpdatedAt
	response.NewsLetters = strings.Split(user.NewsLetters, `;`)

	return c.JSON(fiber.Map{"message": "Recived user data", "data": response})
}
