package handler

import (
	"math/rand"
	"stolowka-api/database"
	"stolowka-api/model"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
)

// GetAllMeals query all products
func GetAllMeals(c *fiber.Ctx) error {
	type Response struct {
		MealDate     time.Time `json:"mealDate"`
		FirstCourse  string    `json:"firstCourse"`
		SecondCourse string    `json:"secondCourse"`
		Tags         []string  `json:"tags"`
	}
	db := database.DB
	var meals []model.Meal
	currentTime := time.Now()
	nextWeek := time.Now().Add(time.Hour * 24 * 7)
	db.Where("meal_date BETWEEN ? AND ?", currentTime, nextWeek).Find(&meals)

	var mealsResponses []Response

	for i := 0; i < len(meals); i++ {
		var mealsResponse Response
		mealsResponse.MealDate = meals[i].MealDate
		mealsResponse.FirstCourse = meals[i].FirstCourse
		mealsResponse.SecondCourse = meals[i].SecondCourse
		if meals[i].Tags != "" {
			mealsResponse.Tags = strings.Split(meals[i].Tags, ";")
		} else {
			mealsResponse.Tags = []string{}
		}
		mealsResponses = append(mealsResponses, mealsResponse)
	}

	var response_message strings.Builder
	response_message.WriteString("Meals up to ")
	response_message.WriteString(nextWeek.Format("2006-01-02"))

	return c.JSON(fiber.Map{"message": response_message.String(), "data": mealsResponses})
}

//Update Meal
func UpdateMeal(c *fiber.Ctx) error {
	type UpdateMealInput struct {
		MealDate     time.Time `json:"mealDate"`
		FirstCourse  string    `json:"firstCourse"`
		SecondCourse string    `json:"secondCourse"`
		Tags         []string  `json:"tags"`
	}
	umi := UpdateMealInput{}
	if err := c.BodyParser(&umi); err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Review your input", "data": err})
	}

	db := database.DB
	meal := model.Meal{}

	db.Find(&meal, "meal_date = ?", umi.MealDate)
	meal.MealDate = umi.MealDate
	meal.FirstCourse = umi.FirstCourse
	meal.SecondCourse = umi.SecondCourse
	meal.Tags = strings.Join(umi.Tags, `;`)
	db.Save(&meal)

	return c.JSON(fiber.Map{"message": "Meal successfully updated", "data": meal})
}

// Create Meal
func CreateMeal(c *fiber.Ctx) error {
	type CreateMeallInput struct {
		MealDate     time.Time `json:"mealDate"`
		FirstCourse  string    `json:"firstCourse"`
		SecondCourse string    `json:"secondCourse"`
		Tags         []string  `json:"tags"`
	}
	cmi := CreateMeallInput{}
	if err := c.BodyParser(&cmi); err != nil {
		return c.Status(500).JSON(fiber.Map{"message": "Review your input", "data": err})
	}

	db := database.DB
	meal := model.Meal{}

	meal.MealDate = cmi.MealDate
	meal.FirstCourse = cmi.FirstCourse
	meal.SecondCourse = cmi.SecondCourse
	meal.Tags = strings.Join(cmi.Tags, `;`)

	db.Save(&meal)

	return c.JSON(fiber.Map{"message": "Meal successfully updated", "data": meal})
}

// Create Meal
func CreateRandomMeals(c *fiber.Ctx) error {
	token := c.Locals("user").(*jwt.Token)

	uid := token.Claims.(jwt.MapClaims)["user_id"]

	db := database.DB
	user := model.User{}
	db.Find(&user, "id = ?", uid)

	for _, permission := range strings.Split(user.Scope, `;`) {
		if permission == "admin" {
			meal := model.Meal{}

			// TODO: Random meal names, for every date, with random tags
			firstCourses := []string{"Zupa pomidorowa", "Zupa kompotowa", "Bagietki czosnkowe", "Zupa krem z dyni"}
			SecondCourses := []string{"Kotlet schabowy z ziemniakami", "Placki ziemniaczane", "Kotlety rybne", "Sezonowe szaleństwo", "Pizza na grubym cieście"}
			TagsList := []string{"Wegetariańskie", "Niskokaloryczne", "Wodniste", "Zawiera orzechy"}

			// get the current year
			year, _, _ := time.Now().Date()

			for i := 1; i <= 12; i++ {
				// get the number of days of the current month
				t := time.Date(year, time.Month(i), 0, 0, 0, 0, 0, time.UTC)

				// loop each day of the month
				for day := 1; day <= t.Day(); day++ {
					t := time.Date(year, time.Month(i), day, 0, 0, 0, 0, time.UTC)
					randomIndexFirst := rand.Intn(len(firstCourses))
					randomIndexSecond := rand.Intn(len(SecondCourses))
					TagsAmount := rand.Intn(len(TagsList))
					meal.MealDate = t
					meal.FirstCourse = firstCourses[randomIndexFirst]
					meal.SecondCourse = SecondCourses[randomIndexSecond]
					var TagsToSave []string
					for i := 1; i < TagsAmount; i++ {
						TagsToSave = append(TagsToSave, TagsList[rand.Intn(len(TagsList))])
					}
					meal.Tags = strings.Join(TagsToSave, `;`)

					db.Save(&meal)
				}
			}

			return c.JSON(fiber.Map{"message": "Meals successfully updated", "data": nil})
		}
	}

	return c.JSON(fiber.Map{"message": "You don't have required permissions", "data": nil})
}

// TODO: Get meals for current week. Create random meals script, Edit Meals Script. Currently works...
