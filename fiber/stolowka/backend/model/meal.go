package model

import "time"

// Meal struct
type Meal struct {
	MealDate     time.Time `gorm:"primary_key" json:"mealDate"`
	FirstCourse  string    `json:"firstCourse"`
	SecondCourse string    `json:"secondCourse"`
	Tags         string    `json:"tags"`
}
