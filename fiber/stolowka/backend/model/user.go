package model

import (
	"time"
)

// User struct
type User struct {
	ID          string     `gorm:"primary_key" json:"id"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `gorm:"index" json:"deletedAt"`
	Scope       string     `json:"scope"`
	NewsLetters string     `json:"newsLetters"`
	Username    string     `gorm:"unique_index;not null" json:"username"`
	Email       string     `gorm:"unique_index;not null" json:"email"`
	Password    string     `gorm:"not null" json:"password"`
	FirstName   string     `json:"firstName"`
	LastName    string     `json:"lastName"`
}
