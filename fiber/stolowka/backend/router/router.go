package router

import (
	"stolowka-api/handler"
	"stolowka-api/middleware"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

// SetupRoutes setup router api
func SetupRoutes(app *fiber.App) {
	// Middleware
	api := app.Group("/api/v1", logger.New())
	api.Get("/health", handler.Hello)

	// Auth
	auth := api.Group("/auth")
	auth.Post("/login", handler.Login)

	// User
	user := api.Group("/user")
	user.Get("/me", middleware.Protected(), handler.GetMyData)
	user.Get("/:id", handler.GetUser)
	user.Post("/", handler.CreateUser)
	user.Post("/admin", handler.CreateAdminUser)
	// user.Patch("/:id", middleware.Protected(), handler.UpdateUser)
	user.Delete("/:id", middleware.Protected(), handler.DeleteUser)

	// Product
	product := api.Group("/meal")
	product.Get("/", middleware.Protected(), handler.GetAllMeals)
	product.Post("/random", middleware.Protected(), handler.CreateRandomMeals)
}
