module stolowka-api

go 1.17

require (
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/jwt/v3 v3.0.2
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/google/uuid v1.3.0
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.29.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
)
