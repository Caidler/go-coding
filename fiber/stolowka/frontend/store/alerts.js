export const state = () => ({
  snackbar: false,
  text: '',
  timeout: 3500
})

export const mutations = {
  openSnackbar (state, text) {
    state.snackbar = true
    state.text = text
  },
  closeSnackbar (state) {
    state.snackbar = false
    state.text = ''
  },
  setSnackbarState (state, snackbarState) {
    state.snackbar = snackbarState
  }
}
