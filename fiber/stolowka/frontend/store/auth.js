export const state = () => ({
  accessToken: localStorage.getItem('accessToken')
})

export const mutations = {
  setAccesToken (state, token) {
    state.accessToken = token
    localStorage.setItem('accessToken', token)
  },
  removeToken (state) {
    state.accessToken = ''
    localStorage.removeItem('accessToken')
  }
}
