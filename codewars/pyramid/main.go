package main

import "fmt"

func PyramidAlternative(n int) [][]int {
	row := [][]int{}
	cell := []int{}

	for i := 0; i < n; i++ {
		cell = append(cell, 1)
		row = append(row, cell)
	}

	return row
}

func Pyramid(n int) [][]int {
	pyramidConstant := 1
	anwser := [][]int{}
	if n > 0 {
		for i := 1; i <= n; i++ {
			newSlice := make([]int, i)
			for i := range newSlice {
				newSlice[i] = pyramidConstant
			}
			anwser = append(anwser, newSlice)
		}
		return anwser
	} else {
		return anwser
	}
}

func main() {
	fmt.Println(PyramidAlternative(2))
}
