package main

import "fmt"

// fib returns a function that returns
// successive Fibonacci numbers.
func fib() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return a
	}
}

func Tribonacci(signature [3]float64, n int) []float64 {
	anwser := []float64{}
	for index, value := range signature {
		if index < n {
			anwser = append(anwser, value)
		}
	}
	for len(anwser) < n {
		last3 := anwser[len(anwser)-3:]
		var nextValue float64 = 0
		for _, s := range last3 {
			nextValue = nextValue + s
		}
		anwser = append(anwser, nextValue)
	}
	return anwser
}

func TribonacciAlternative(signature [3]float64, n int) (r []float64) {
	r = signature[:]
	for i := 0; i < n; i++ {
		l := len(r)
		r = append(r, r[l-1]+r[l-2]+r[l-3])
	}
	return r[:n]
}

func TribonacciAlternative2(signature [3]float64, n int) []float64 {
	result := make([]float64, n)
	for i := 0; i < 3 && i < n; i++ {
		result[i] = signature[i]
	}
	for i := 3; i < n; i++ {
		result[i] = result[i-1] + result[i-2] + result[i-3]
	}
	return result
}

func main() {
	fmt.Println(Tribonacci([3]float64{1, 1, 1}, 10))
}
