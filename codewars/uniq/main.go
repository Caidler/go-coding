package main

import "fmt"

func Uniq(strings []string) []string {
	i := 0
	stringsLenght := len(strings) - 1
	anwser := []string{}
	if len(strings) > 0 {
		lastElement := strings[i]
		i++
		for i < stringsLenght {
			if lastElement != strings[i] {
				anwser = append(anwser, lastElement)
			}
			lastElement = strings[i]
			i++
		}
		anwser = append(anwser, lastElement)
	}
	return anwser
}

func UniqAlternative(a []string) []string {
	b := make([]string, 0)
	if len(a) > 0 {
		b = append(b, a[0])
	}
	for j := 1; j < len(a); j++ {
		if a[j-1] != a[j] {
			b = append(b, a[j])
		}
	}
	return b
}

func main() {
	fmt.Println(Uniq([]string{"a", "a", "b", "b", "c", "a", "b", "c", "c"}))
}
