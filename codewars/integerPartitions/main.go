package main

import "fmt"

func Partitions(n int) int {
	parts := make([]int, n+1)
	parts[0] = 1
	counter := 0
	for i := 1; i < n+1; i++ {
		counter = 0
		for j := i; j < n+1; j++ {
			parts[j] += parts[counter]
			counter++
		}
	}
	return parts[n]
}

func main() {
	for i := 1; i <= 5; i++ {
		fmt.Println(i, Partitions(i))
	}
}
