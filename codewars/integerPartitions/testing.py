# Credit: https://codereview.stackexchange.com/a/202852

def partitions(n):
    parts = [1]+[0]*n
    print(n, len(parts))
    for t in range(1, n+1):
        for i, x in enumerate(range(t, n+1)):
            parts[x] += parts[i]
    return parts[n]


for i in range(1, 5):
    print(i, partitions(i))
