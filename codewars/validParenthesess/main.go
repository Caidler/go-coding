package main

import "fmt"

func ValidParentheses(parens string) bool {
	opening := '('
	closing := ')'
	balance := 0

	for _, char := range parens {
		if char == opening {
			balance += 1
		} else if char == closing {
			balance -= 1
			if balance < 0 {
				return false
			}
		}
	}
	return balance == 0
}

func main() {
	fmt.Println(ValidParentheses("()()()()(())(())))((()"))
}
