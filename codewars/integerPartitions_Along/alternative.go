package main

import (
	"fmt"
	"sort"
)

// map[key]value -> key:int, value:map[int][bool]
// note: map[key][bool] can be used to impl a Set
var cache map[int]map[int]bool

func partition(n int) []int {
	if cache == nil {
		cache = map[int]map[int]bool{}
	}
	_, ok := cache[n]
	if !ok {
		xs := map[int]bool{}
		xs[1] = true
		for i := 1; i <= n; i++ {
			ys := partition(n - i)
			for _, t := range ys {
				k := t * i
				_, ok := xs[k]
				if !ok {
					xs[k] = true
				}
			}
		}
		cache[n] = xs
	}
	var ls []int
	for key, _ := range cache[n] {
		ls = append(ls, key)
	}
	sort.Slice(ls, func(i, j int) bool {
		return ls[i] < ls[j]
	})
	return ls
}

func Part(n int) string {
	ls := partition(n)
	fmt.Println(ls)
	l := len(ls)
	r := ls[l-1] - ls[0]
	s := 0
	for _, x := range ls {
		s += x
	}
	u := (float64(s)) / (float64(l))
	m := 0.5 * (float64(ls[l/2] + ls[(l-1)/2]))
	return fmt.Sprintf("Range: %d Average: %.2f Median: %.2f", r, u, m)
}

func main() {
	for i := 1; i < 10; i++ {
		fmt.Println(Part(i))
	}
}
