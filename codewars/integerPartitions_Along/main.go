package main

import (
	"fmt"
	"sort"
)

func removeDuplicateInt(intSlice []int) []int {
	// Credit: https://stackoverflow.com/questions/66643946/how-to-remove-duplicates-strings-or-int-from-slice-in-go
	allKeys := make(map[int]bool)
	list := []int{}
	for _, item := range intSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func integerPartition(n int) [][]int {
	// Credit: https://stackoverflow.com/questions/66643946/how-to-remove-duplicates-strings-or-int-from-slice-in-go
	// Credit: https://gist.github.com/jonbodner/2676533
	var anwser [][]int
	set := make(map[string]bool)
	for i := 1; i <= n; i++ {
		for _, result := range worker([]int{i}, n-i) {
			if set[fmt.Sprintf("%v", result)] == false {
				set[fmt.Sprintf("%v", result)] = true
				anwser = append(anwser, result)
			}
		}
	}
	return anwser
}

func worker(already []int, val int) [][]int {
	if val == 0 {
		sort.Ints(already)
		return [][]int{already}
	}
	out := [][]int{}
	for i := 1; i <= val; i++ {
		out = append(out, worker(append(already, i), val-i)...)
	}
	return out
}

func Part(n int) string {
	if n > 0 {
		partitions := integerPartition(n)
		fmt.Println("Current n: ", n)
		fmt.Println(partitions)
		productPartitions := []int{}
		for i := 0; i < len(partitions); i++ {
			arrayProduct := 1
			for j := 0; j < len(partitions[i]); j++ {
				arrayProduct *= partitions[i][j]
			}
			productPartitions = append(productPartitions, arrayProduct)
		}
		productPartitions = removeDuplicateInt(productPartitions)
		sort.Ints(productPartitions)
		partitionsRange := productPartitions[len(productPartitions)-1] - productPartitions[0]
		partitionsSum := 0
		for i := 0; i < len(productPartitions); i++ {
			partitionsSum += productPartitions[i]
		}
		partitionsAvarage := float64(partitionsSum) / float64(len(productPartitions))
		var partitionsMedian float64
		if len(productPartitions)%2 == 0 {
			partitionsMedian = (float64(productPartitions[(len(productPartitions))/2-1]) + float64(productPartitions[(len(productPartitions))/2])) / 2
		} else {
			partitionsMedian = float64(productPartitions[len(productPartitions)/2])
		}
		return "Range: " + fmt.Sprintf("%d", partitionsRange) + " Average: " + fmt.Sprintf("%.2f", partitionsAvarage) + " Median: " + fmt.Sprintf("%.2f", partitionsMedian)
	}
	panic("N is lower then 1")
}

func main() {
	for i := 1; i < 10; i++ {
		fmt.Println(Part(i))
	}
}
