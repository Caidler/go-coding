# Credit: https://codereview.stackexchange.com/a/202852

def partitions(n):
    parts = [1]+[0]*n
    for t in range(1, n+1):
        for i, x in enumerate(range(t, n+1)):
            parts[x] += parts[i]
    print(parts)
    return parts[n]

print(partitions(50))


# Credit: https://stackoverflow.com/a/44209393/15204712

def partitions(n, I=1):
    yield (n,)
    for i in range(I, n//2 + 1):
        for p in partitions(n-i, i):
            yield (i,) + p

for partition in partitions(10, I=1):
    print(partition)


# A utility function to print an
# array p[] of size 'n'
def printArray(p, n):
	for i in range(0, n):
		print(p[i], end = " ")
	print()

def printAllUniqueParts(n):
	p = [0] * n	 # An array to store a partition
	k = 0		 # Index of last element in a partition
	p[k] = n	 # Initialize first partition
				# as number itself

	# This loop first prints current partition,
	# then generates next partition.The loop
	# stops when the current partition has all 1s
	while True:
			printArray(p, k + 1)

			# Generate next partition

			# Find the rightmost non-one value in p[].
			# Also, update the rem_val so that we know
			# how much value can be accommodated
			rem_val = 0
			while k >= 0 and p[k] == 1:
				rem_val += p[k]
				k -= 1

			# if k < 0, all the values are 1 so
			# there are no more partitions
			if k < 0:
				print()
				return

			# Decrease the p[k] found above
			# and adjust the rem_val
			p[k] -= 1
			rem_val += 1

			# If rem_val is more, then the sorted
			# order is violated. Divide rem_val in
			# different values of size p[k] and copy
			# these values at different positions after p[k]
			while rem_val > p[k]:
				p[k + 1] = p[k]
				rem_val = rem_val - p[k]
				k += 1

			# Copy rem_val to next position
			# and increment position
			p[k + 1] = rem_val
			k += 1

# Driver Code
print('All Unique Partitions of 2')
printAllUniqueParts(2)

print('All Unique Partitions of 3')
printAllUniqueParts(3)

print('All Unique Partitions of 4')
printAllUniqueParts(4)

# This code is contributed
# by JoshuaWorthington
