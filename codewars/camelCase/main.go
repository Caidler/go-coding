package main

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
)

func IsUpper(s string) bool {
	for _, r := range s {
		if !unicode.IsUpper(r) && unicode.IsLetter(r) {
			return false
		} else {
			return true
		}
	}
	return true
}

func Splitter(s string, splits string) []string {
	m := make(map[rune]int)
	for _, r := range splits {
		m[r] = 1
	}

	splitter := func(r rune) bool {
		return m[r] == 1
	}

	return strings.FieldsFunc(s, splitter)
}

func ToCamelCase(testingString string) string {
	if testingString != "" {
		// words := Splitter(testingString, "_-") // Splitter option
		words := regexp.MustCompile("-|_").Split(testingString, -1) // Regex option
		var firstItem []string
		firstItem = append(firstItem, words[0])
		words = words[1:]
		for i, _ := range words {
			words[i] = strings.Title(words[i])
		}
		words = append(firstItem, words...)
		return strings.Join(words, "")
	}
	return testingString
}

func main() {
	var testingString string = "desert-south"
	fmt.Println(ToCamelCase((testingString)))
}
