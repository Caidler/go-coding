package main

import (
	"fmt"
	"strconv"
	"strings"
)

func PrinterError_Original(s string) string {
	var errors int = 0
	runes := []rune(s)
	for i := 0; i < len(runes); i++ {
		if runes[i] <= 109 {
			fmt.Println(runes[i])
		} else {
			errors += 1
		}
	}
	return log("{errors}/{chars}", "{errors}", strconv.Itoa(errors), "{chars}", strconv.Itoa(len(runes)))
}

func log(format string, args ...string) string {
	r := strings.NewReplacer(args...)
	return r.Replace(format)
}

func PrinterError(s string) string {
	errors := 0
	for _, c := range s {
		if string(c) > "m" {
			errors++
		}
	}
	return fmt.Sprintf("%d/%d", errors, len(s))
}

func main() {
	fmt.Println(PrinterError("abcdefghijklmxxx"))
}
