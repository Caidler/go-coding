package main

import (
	"fmt"
	"math"
)

func Decompose(n int64) []int64 {
	decomposeTarget := n * n
	var decomposeSum int64 = 0
	anwser := []int64{}
	for i := n - 1; i > 0; i-- {
		if decomposeSum+i*i <= decomposeTarget {
			decomposeSum += i * i
			anwser = append(anwser, i)
			fmt.Println(decomposeSum, decomposeTarget, i)
		}
	}
	if decomposeSum == decomposeTarget {
		return anwser
	} else {
		return []int64{}
	}
}

// Credit: https://stackoverflow.com/a/64544238/15204712
func DecomposeAlternative(n int64) []int64 {
	n = n * n
	results := make([]int64, n+1)
	for i := 0; i < len(results); i++ {
		results[i] = -1
	}

	results[1] = 1
	for i := int64(2); i <= n; i++ {
		for j := int64(math.Sqrt(float64(i))); j >= 1; j-- {
			if results[i-int64(j*j)] != -1 && results[i-int64(j*j)] < int64(j) {
				results[i] = int64(j)
				break
			}
		}
	}

	anwser := []int64{}
	current := n

	for results[current] != -1 {
		anwser = append(anwser, results[current])
		current -= results[current] * results[current]
	}
	for i, j := 0, len(anwser)-1; i < j; i, j = i+1, j-1 {
		anwser[i], anwser[j] = anwser[j], anwser[i]
	}
	fmt.Println(results)
	return anwser
}

func DecomposeAnwser1(n int64) []int64 {
	r := calcuateD(n, n*n)
	if len(r) > 0 {
		return r[:len(r)-1]
	}
	return []int64{}
}

func calcuateD(n, r int64) []int64 {
	if r < 1 {
		return []int64{n}
	}

	for i := n - 1; i > 0; i-- {
		if (r - (i * i)) >= 0 {
			a := calcuateD(i, (r - (i * i)))
			if a != nil {
				a = append(a, n)
				return a
			}
		}
	}

	return nil
}

func loop(s int64, i int64) []int64 {
	if s < 0 {
		return nil
	}
	if s == 0 {
		return []int64{}
	}
	var j int64 = i - 1
	for ; j > 0; j-- {
		var sub = loop(s-j*j, j)
		if sub != nil {
			return append(sub, []int64{j}...)
		}
	}
	return nil
}
func DecomposeAnwser2(n int64) []int64 {
	return loop(n*n, n)
}

func main() {
	fmt.Println(DecomposeAnwser2(5))
}
