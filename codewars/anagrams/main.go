package main

import "fmt"

func Anagrams(word string, words []string) []string {
	var anwser []string
	mainHash := make(map[string]int)

	for _, rune := range word {
		j := mainHash[string(rune)]

		if j == 0 {
			mainHash[string(rune)] = 1
		} else {
			mainHash[string(rune)] = j + 1
		}
	}

	for _, elementWord := range words {
		localHash := make(map[string]int)
		for _, rune := range elementWord {
			j := localHash[string(rune)]

			if j == 0 {
				localHash[string(rune)] = 1
			} else {
				localHash[string(rune)] = j + 1
			}
		}
		isAnagram := true
		for key := range localHash {
			if localHash[key] != mainHash[key] {
				isAnagram = false
			}
		}
		if isAnagram {
			anwser = append(anwser, elementWord)
		}
	}

	return anwser
}

func main() {
	fmt.Println(Anagrams("racer", []string{"aabb", "abcd", "bbaa", "racers"}))
}
