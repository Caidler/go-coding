package main

import "fmt"

func CreateSpiral(n int) [][]int {
	if n <= 0 {
		return [][]int{}
	}

	result := make([][]int, n)
	for i := 0; i < n; i++ {
		result[i] = make([]int, n)
	}

	for i, j, start := 0, 0, 1; n > 0; i, j = i+1, j+1 {
		start = fillData(i, j, start, n, result)
		n -= 2
	}

	return result
}

func fillData(i, j, start, n int, result [][]int) int {
	if n == 1 {
		result[i][j] = start
		return start
	}

	row, column := i, j
	for ; column < j+n-1; column++ {
		result[i][column] = start
		start++
	}

	for ; row < n+i-1; row++ {
		result[row][column] = start
		start++
	}

	for ; column > j; column-- {
		result[row][column] = start
		start++
	}

	for ; row > i; row-- {
		result[row][column] = start
		start++
	}

	return start
}

func CreateSpiralAlternative(n int) [][]int {
	if n < 1 {
		return [][]int{}
	}
	anwserArray := make([][]int, n)
	for i := 0; i < n; i++ {
		anwserArray[i] = make([]int, n)
	}
	anwserArray[0][0] = 1
	valueToInsert := 2
	top, bot, left, right := 0, n, 0, n
	x, y := 0, 0
	for left < right {
		for x++; x < right; x++ {
			anwserArray[y][x] = valueToInsert
			valueToInsert++
		}
		x--
		right--
		for y++; y < bot; y++ {
			anwserArray[y][x] = valueToInsert
			valueToInsert++
		}
		bot--
		y--
		for x--; x >= left; x-- {
			anwserArray[y][x] = valueToInsert
			valueToInsert++
		}
		left++
		x++
		for y--; y > top; y-- {
			anwserArray[y][x] = valueToInsert
			valueToInsert++
		}
		y++
		top++
	}
	return anwserArray
}

func main() {
	fmt.Println(CreateSpiralAlternative(3))
}
