package main

import (
	"net"
)

func Is_valid_ip(ip string) bool {
	if net.ParseIP(ip) == nil {
		return false
	} else {
		return true
	}
}
