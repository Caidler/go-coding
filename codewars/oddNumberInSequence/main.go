package main

import "fmt"

func FindOdd(seq []int) int {
	numbersMapping := make(map[int]int)
	for _, number := range seq {
		// Credit: https://stackoverflow.com/a/2050629/15204712
		if _, ok := numbersMapping[number]; ok {
			numbersMapping[number] += 1
		} else {
			numbersMapping[number] = 1
		}
	}
	for index, number := range numbersMapping {
		if number%2 != 0 {
			return index
		}
	}
	return 0
}

func FindOddAlternative(seq []int) int {
	res := 0
	for _, x := range seq {
		res ^= x
	}
	return res
}

func FindOddAlternative2(seq []int) int {
	// probably best implementation

	m := make(map[int]int)

	for _, e := range seq {
		m[e]++
	}

	for k, v := range m {
		if v%2 != 0 {
			return k
		}
	}
	return 0
}

func main() {
	numbers := []int{1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1}
	fmt.Println(FindOddAlternative2(numbers))
}
