package main

import (
	"bytes"
	"fmt"
	"math"
	"strconv"
)

func PrimeFactors(n int) string {
	var anwser bytes.Buffer
	powerMultiplier := 0
	for n%2 == 0 {
		powerMultiplier++
		n /= 2
	}
	if powerMultiplier == 1 {
		anwser.WriteString("(")
		anwser.WriteString(strconv.Itoa(2))
		anwser.WriteString(")")
		powerMultiplier = 0
	}
	if powerMultiplier > 1 {
		anwser.WriteString("(")
		anwser.WriteString(strconv.Itoa(2))
		anwser.WriteString("**")
		anwser.WriteString(strconv.Itoa(powerMultiplier))
		anwser.WriteString(")")
		powerMultiplier = 0
	}
	for i := 3; i <= int(math.Sqrt(float64(n))); i += 2 {
		for n%i == 0 {
			powerMultiplier++
			n /= i
		}
		if powerMultiplier == 1 {
			anwser.WriteString("(")
			anwser.WriteString(strconv.Itoa(i))
			anwser.WriteString(")")
			powerMultiplier = 0
		}
		if powerMultiplier > 1 {
			anwser.WriteString("(")
			anwser.WriteString(strconv.Itoa(i))
			anwser.WriteString("**")
			anwser.WriteString(strconv.Itoa(powerMultiplier))
			anwser.WriteString(")")
			powerMultiplier = 0
		}
	}
	if n > 2 {
		anwser.WriteString("(")
		anwser.WriteString(strconv.Itoa(n))
		anwser.WriteString(")")
	}
	return anwser.String()
}

func PrimeFactorsAlternative(n int) string {
	res := ""
	fac := 2
	var nn int = n
	for fac <= n {
		count := 0
		for nn%fac == 0 {
			count++
			nn /= fac
		}
		if count > 0 {
			res += "(" + strconv.Itoa(fac)
			if count > 1 {
				res += "**" + strconv.Itoa(count)
			}
			res += ")"
		}
		fac++
	}
	return res
}

func main() {
	fmt.Println(PrimeFactors(7775460))
}
