package main

import (
	"fmt"
	"strings"
)

func ReverseWord(str string) string {
	runes := []rune(str)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func ReverseWords(str string) string {
	anwser_list := strings.Split(str, " ")
	for index, anwser := range anwser_list {
		anwser_list[index] = ReverseWord(anwser)
	}
	return strings.Join(anwser_list, " ")
}

func main() {
	testingString := "double  spaced  words"
	expectedOutput := "elbuod  decaps  sdrow"
	fmt.Println(ReverseWords(testingString))
	fmt.Println(ReverseWords(testingString) == expectedOutput)
}
