package main

import "fmt"

func MoveZeros(arr []int) []int {
	valueToSearch := 0
	valuesCounter := 0
	anwser := []int{}
	for i := range arr {
		if arr[i] == valueToSearch {
			valuesCounter++
		} else {
			anwser = append(anwser, arr[i])
		}
	}
	for i := 0; i < valuesCounter; i++ {
		anwser = append(anwser, valueToSearch)
	}
	return anwser
}

func MoveZerosAlternative(arr []int) []int {
	i := 0
	valueToSearch := 0
	valuesCounter := 0

	for i < (len(arr) - valuesCounter) {
		if arr[i] == valueToSearch {
			arr = append(arr[:i], arr[i+1:]...)
			arr = append(arr, valueToSearch)
			valuesCounter++
			continue
		}
		i++
	}

	return arr
}

func main() {
	fmt.Println(MoveZerosAlternative([]int{1, 2, 0, 1, 0, 1, 0, 3, 0, 1}))
}
