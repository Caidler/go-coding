package main

import (
	"fmt"
	"strconv"
	"strings"
)

func CreatePhoneNumber(numbers [10]uint) string {
	result := make([]string, len(numbers))
	for index, number := range numbers {
		result[index] = strconv.FormatInt(int64(number), 10)
	}
	firstPart := strings.Join(result[0:3], "")
	secondPart := strings.Join(result[3:6], "")
	thirdPart := strings.Join(result[6:10], "")
	return fmt.Sprintf("(%s) %s-%s", firstPart, secondPart, thirdPart)
}

func CreatePhoneNumberAlternative(n [10]uint) string {
	return fmt.Sprintf("(%d%d%d) %d%d%d-%d%d%d%d", n[0], n[1], n[2], n[3], n[4], n[5], n[6], n[7], n[8], n[9])
}

func main() {
	numbers := [10]uint{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	fmt.Println(CreatePhoneNumber(numbers))
}
