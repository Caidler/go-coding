package main

import (
	"fmt"
	"math"
)

func getTenPower() {
	for i := 0; i < 10; i++ {
		tenPower := math.Pow(10, float64(i))
		tenReminderofThirteen := int(tenPower) % 13
		fmt.Println(tenReminderofThirteen)
	}
}

func reverseInt(n int) int {
	// Credit: https://stackoverflow.com/a/35973757/15204712
	new_int := 0
	for n > 0 {
		remainder := n % 10
		new_int *= 10
		new_int += remainder
		n /= 10
	}
	return new_int
}

func Thirt(n int) int {
	remindersPattern := [6]int{1, 10, 9, 12, 3, 4}
	currentReminder := 0
	new_int := 0
	for n > 0 {
		remainder := n % 10
		new_int += remainder * remindersPattern[currentReminder]
		if currentReminder == len(remindersPattern)-1 {
			currentReminder = 0
		} else {
			currentReminder += 1
		}
		n /= 10
	}
	return new_int
}

func ThirtHandler(n int) int {
	nextValue := 0
	currentValue := Thirt(n)
	for nextValue != currentValue {
		currentValue = Thirt(currentValue)
		nextValue = Thirt(currentValue)
	}
	return currentValue
}

func ThirtAlternative(n int) int {
	coefficients := [6]int{1, 10, 9, 12, 3, 4}
	newInput := 0
	iter := 0
	m := n
	for ; m > 0; m /= 10 {
		newInput += m % 10 * coefficients[iter%6]
		iter++
	}
	if newInput == n {
		return n
	} else {
		return Thirt(newInput)
	}
}

func main() {
	number := 1234567
	fmt.Println(ThirtAlternative(number))
}
